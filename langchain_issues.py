from langchain.agents import create_tool_calling_agent, AgentExecutor, initialize_agent, AgentType
from langchain.prompts import ChatPromptTemplate
from tools import fetch_issue, clone_repository, run_command, read_file, write_file, create_branch_commit_and_push, create_gitlab_merge_request

from langchain_anthropic import ChatAnthropic
llm = ChatAnthropic(model_name="claude-3-haiku-20240307")

chat_template = ChatPromptTemplate.from_messages(
    [
        ("system", """
         You have the software engineering capabilities of a Principle engineer. 
         You can clone a repository, run commands and make changes to files. 
         Make sure you follow test driven development and write tests to verify the functionality that you are building.
         Please write code in only the language that is specified in the repository.
         Make a plan on what needs to be done. The plan should include
         1. Understand the issue
         2. Clone the repository
         3. Read code files to verify the issue
         5. Fix issue by writing files
         6. Write tests to confirm that your program works
         7. Push changes to the repository
         8. Create a merge request for the change
         """),
        ("placeholder", "{chat_history}"),
        ("human", "Can you help fix the following issue: {input}."),
        ("placeholder", "{agent_scratchpad}"),
    ]
)

tools = [
    fetch_issue, clone_repository, run_command, read_file, 
    write_file, create_branch_commit_and_push, create_gitlab_merge_request]

agent = create_tool_calling_agent(llm=llm, tools=tools, prompt=chat_template)
agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True)
agent_executor.invoke({"input": "Project ID: 56947686 , Issue: 1"})
