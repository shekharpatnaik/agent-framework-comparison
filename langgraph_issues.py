from langchain_anthropic import ChatAnthropic
from langchain_core.messages import HumanMessage, BaseMessage, ToolMessage, SystemMessage
from typing import TypedDict, List
from langgraph.graph import END, StateGraph
from tools import fetch_issue, clone_repository, run_command, read_file, write_file, create_branch_commit_and_push, create_gitlab_merge_request

llm = ChatAnthropic(model_name="claude-3-sonnet-20240229")

pm_tools = [
    fetch_issue
]

developer_tools = [
    clone_repository, run_command, read_file, 
    write_file, create_branch_commit_and_push, create_gitlab_merge_request]

pm_model = llm.bind_tools(tools=pm_tools)
developer_model = llm.bind_tools(tools=developer_tools)

class AgentState(TypedDict):
    project_id: str
    issue_id: str
    plan: str
    developer_chat_history: List[BaseMessage]
    planner_chat_history: List[BaseMessage]
    issue_description: str

def call_planner(state: AgentState):
    if state["plan"] == "":
        state["planner_chat_history"] = [
            SystemMessage(content="""
            You are an experienced code planner. Your role is to come up with a plan to help the developer solve the user's issue.
            Make sure you promote best practices such as test driven development to the developer.
            An example plan is as follows:
            1. Understand the issue
            2. Clone the repository
            3. Read code files to verify the issue
            5. Fix issue by writing files
            6. Write tests to confirm that your program works
            7. Push changes to the repository
            8. Create a merge request for the change          
            """),
            HumanMessage(content=f"Can you help fix the following issue: Project ID: {state["project_id"]}, Issue ID: {state['issue_id']}.")
        ]

    print("Planner is thinking...")
    result = pm_model.invoke(state["planner_chat_history"])
    state["plan"] = result.content
    print(f"Plan is created {state['plan']}")
    state["planner_chat_history"].append(result)
    return state

def call_developer(state: AgentState):
    if not(state["developer_chat_history"]) or len(state["developer_chat_history"]) == 0:
        state["developer_chat_history"] = [
            SystemMessage(content=f"""
            You are a very experienced staff engineer. You need to write code to solve the users issue.
            This is plan that the project manager has come up with
            {state["plan"]}

            You can clone a repository, run commands and make changes to files.
            Please write code in only the language that is specified in the repository. You can run `ls` or a similar command to figure out the language.
            Make sure you follow test driven development and write tests to verify the functionality that you are building.

            Once you are done please respond with "TASK_COMPLETE"
            """),
            HumanMessage(content=f"Can you help fix the following issue: {state['issue_description']}")
        ]

    print("Developer is thinking...")
    result = developer_model.invoke(state["developer_chat_history"])
    print(f"Result from developer: {result}")
    state["developer_chat_history"].append(result)
    return state

def invoke_planner_tools(state: AgentState):
    last_message = state["planner_chat_history"][-1]
    tool_calls = last_message.tool_calls if last_message.tool_calls else []
    for tool_call in tool_calls:
        if tool_call['name'] == 'fetch_issue':
            print("Calling fetch issue tool")
            res = fetch_issue.invoke(tool_call['args'])
            
            state["issue_description"] = res

            state["planner_chat_history"].append(ToolMessage(
                tool_call_id=tool_call.get("id"),
                content=res
            ))

            return state
        
def invoke_developer_tools(state: AgentState):
    print('In invoke developer tools')
    last_message = state["developer_chat_history"][-1]
    tool_calls = last_message.tool_calls if last_message.tool_calls else []
    for tool_call in tool_calls:
        tool_fn = None
        print(f"Calling developer tool with name {tool_call['name']}")
        if tool_call['name'] == 'clone_repository':
            tool_fn = clone_repository
        elif tool_call['name'] == 'run_command':
            tool_fn = run_command
        elif tool_call['name'] == 'read_file':
            tool_fn = read_file
        elif tool_call['name'] == 'write_file':
            tool_fn = write_file
        elif tool_call['name'] == 'create_branch_commit_and_push':
            tool_fn = create_branch_commit_and_push
        elif tool_call['name'] == 'create_gitlab_merge_request':
            tool_fn = create_gitlab_merge_request
        else:
            raise Exception(f"Unknown tool {tool_call['name']}")
        
        res = tool_fn.invoke(tool_call['args'])
            
        state["developer_chat_history"].append(ToolMessage(
            tool_call_id=tool_call.get("id"),
            content=res
        ))

    return state
        
def planner_router(state: AgentState):
    last_message = state["planner_chat_history"][-1]
    tool_calls = last_message.tool_calls if last_message.tool_calls else []
    if len(tool_calls) > 0:
        return "planner_tools"
    else:
        return "developer"

def developer_router(state: AgentState):
    last_message = state["developer_chat_history"][-1]
    if "TASK_COMPLETE" in last_message.content:
        return "end"
    tool_calls = last_message.tool_calls if last_message.tool_calls else []
    if len(tool_calls) > 0:
        return "developer_tools"
    else:
        return "developer"

graph = StateGraph(AgentState)

graph.add_node('planner', call_planner)
graph.add_node('planner_tools', invoke_planner_tools)
graph.add_node('developer', call_developer)
graph.add_node('developer_tools', invoke_developer_tools)

graph.add_conditional_edges('planner', planner_router, {
    'developer': 'developer',
    'planner_tools': 'planner_tools'
})

graph.add_edge("planner_tools", "planner")
graph.add_edge("developer_tools", "developer")

graph.add_conditional_edges('developer', developer_router, {
    'end': END,
    'developer': 'developer',
    'developer_tools': 'developer_tools'
})

graph.set_entry_point("planner")

runnable = graph.compile()
result = runnable.invoke(AgentState(plan="", chat_history=[], project_id="56947686", issue_id="1"), {"recursion_limit": 100})
print(result["developer_chat_history"][-1])
