from crewai import Agent, Task, Crew, Process
from langchain_openai import ChatOpenAI
from tools import fetch_issue, clone_repository, run_command, read_file, write_file, create_branch_commit_and_push, create_gitlab_merge_request

planner = Agent(
  role='Planner',
  goal='Create an effective plan for fixing the following issue: project_id: {project_id}, issue_id: {issue_id}',
  verbose=True,
  memory=True,
  backstory=(
    """
    You are an experienced code planner. Your role is to come up with a plan to help the developer solve the user's issue.
    Make sure you promote best practices such as test driven development to the developer.
    An example plan is as follows:
    1. Understand the issue
    2. Clone the repository
    3. Read code files to verify the issue
    5. Fix issue by writing files
    6. Write tests to confirm that your program works
    7. Push changes to the repository
    8. Create a merge request for the change        
    """
  ),
  tools=[fetch_issue],
  allow_delegation=True
)

engineer = Agent(
  role='Staff Sofware Engineer',
  goal='You are a very experienced staff engineer. You need to write code to solve the users issue.',
  verbose=True,
  memory=True,
  backstory=(
    """
    You can clone a repository, run commands and make changes to files.
    Please write code in only the language that is specified in the repository. You can run `ls` or a similar command to figure out the language.
    Make sure you follow test driven development and write tests to verify the functionality that you are building.
    """
  ),
  tools=[fetch_issue, clone_repository, run_command, read_file, write_file, create_branch_commit_and_push, create_gitlab_merge_request],
  allow_delegation=False
)

issue_task = Task(
  description=(
    "Fix the following issue for the user: project_id: {project_id}, issue_id: {issue_id}"
  ),
  expected_output='Updated files based on the issue',
  agent=engineer,
)

crew = Crew(
  agents=[engineer],
  tasks=[issue_task],
  process=Process.hierarchical,  # Optional: Sequential task execution is default
  memory=False,
  cache=True,
  max_rpm=100,
  manager_llm=ChatOpenAI(model="gpt-4-turbo")
)

result = crew.kickoff(inputs={'project_id': '56947686', 'issue_id': '1'})
print(result)
