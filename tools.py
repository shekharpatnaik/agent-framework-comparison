import os
import gitlab
import git
import subprocess
import json
from langchain.pydantic_v1 import BaseModel, Field
from typing import Optional
from unique_names_generator import get_random_name
from langchain.tools import tool
from unique_names_generator.data import ADJECTIVES, NAMES

gl = gitlab.Gitlab(private_token=os.getenv("GITLAB_TOKEN"))

random_name = get_random_name(
        combo=[ADJECTIVES, NAMES], separator="_", style="lowercase")

workspace_name = os.path.join("workspaces", random_name)

@tool
def fetch_issue(project_id: str, issue_id: str):
    """Fetch an issue from a GitLab project."""
    project = gl.projects.get(project_id)
    issue = project.issues.get(issue_id)
    return issue.to_json()

@tool
def clone_repository(repository_url: str):
    """Clone a Git repository. Once the repository is cloned you will automatically be in the repository working directory and the main branch will already be checked out."""
    repo = git.Repo.clone_from(repository_url, workspace_name)
    return "Repository cloned."

@tool
def run_command(command: str) -> dict[str, str]:
    """Run a command in the repository working directory."""
    # Change to workspace directory while keeping the original working directory
    original_dir = os.getcwd()
    os.chdir(workspace_name)

    # Run the command and get both stdout and stderr 
    try:
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout = result.stdout
        stderr = result.stderr
    finally:
        os.chdir(original_dir)

    return json.dumps({
        "stdout": stdout,
        "stderr": stderr,
    })

@tool
def read_file(file_path: str) -> str:
    """Read the contents of a file."""
    try:
        actual_path = os.path.join(workspace_name, file_path)
        with open(actual_path, 'r') as f:
            contents = f.read()
    except Exception as e:
        return f"ERROR: {e}"
    return contents

class WriteFileInput(BaseModel):
    file_path: str = Field(description="the relative path of the file to be written. This is required.")
    contents: Optional[str] = Field(description="the contents of the file. This is required.")

@tool("write_file", args_schema=WriteFileInput)
def write_file(file_path: str, contents: str) -> str:
    """Write the given contents to a file. Please specify the file_path and the contents to write."""

    if not(contents):
        return "ERROR: You need to specify the contents of the file."
    try:
        actual_path = os.path.join(workspace_name, file_path)
        with open(actual_path, 'w') as f:
            f.write(contents)
    except Exception as e:
        return f"ERROR: {e}"
    return "SUCCESS"

@tool
def create_branch_commit_and_push(branch_name: str, commit_message: str):
    """Create a branch with specified name, add files, commit and push the changes to the repository.
    The branch name and commit_message should be descriptive and should contain information about
    the fix as well as the issue number. Please run this after the issue is fixed.
    """
    # Change to workspace directory while keeping the original working directory
    # original_dir = os.getcwd()
    # os.chdir(workspace_name)
    repo = git.Repo(workspace_name)
    repo.git.checkout("-b", branch_name)
    repo.git.add(".")
    repo.git.commit("-m", f"'{commit_message}'")
    repo.git.push("origin", branch_name)
    # os.chdir(original_dir)
    return "SUCCESS"

@tool
def create_gitlab_merge_request(project_id: str, branch: str, title: str, description: str):
    """Create a merge request on GitLab."""
    project = gl.projects.get(project_id)
    merge_request = project.mergerequests.create({
        "source_branch": branch,
        "target_branch": "main",
        "title": title,
        "description": description,
    })
    return merge_request.to_json()